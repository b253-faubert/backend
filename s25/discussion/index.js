
// // JSON objects
// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// // JSON arrays
// "cities": [
// 	{ 
// 		"city": "Quezon City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	},
// 	{ 
// 		"city": "Makati City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	},
// 	{ 
// 		"city": "Mandaluyong",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	}
// ]

let batchesArray = [
	{ batchName: "Batch253"},
	{ batchName: "Batch243"}

]

console.log(batchesArray)
console.log("Stringify (serialization) of batches array: ")
console.log(JSON.stringify(batchesArray))

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Cebu City",
		country: "Philippines"
	}
})

console.log(data)

let batchesJSON = `[
	{ "batchName": "Batch253"},
	{ "batchName": "Batch243"}

]`

console.log(batchesJSON)
console.log(JSON.parse(batchesJSON))