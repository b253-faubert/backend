
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}

console.log(cellphone)
console.log(typeof cellphone)

function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptopSam = new Laptop("Lenovo", 2008);
console.log(laptopSam);

let laptopOwen = new Laptop("Macbook Air", [2016, 2020]);
console.log(laptopOwen)

let myComputer = new Object();
console.log(myComputer)

console.log(laptopOwen.name)
console.log(laptopOwen["name"])

let car = {}

car.name = "Honda Civic"
console.log(car)
car["manufactureDate"] = 2019
console.log(car)
delete car.manufactureDate;
console.log(car)

let person = {
	name: "Pedro",
	age: 25,
	talk: function() {
		console.log("Hello! Ako si " + this.name)
	}
}

person.talk()
person.walk = function() {
	console.log(this.name + " has walked 25 steps forward")
}

person.walk()

let friend = {
	firstName: "Maria",
	lastName: "Dela Cruz",
	address: {
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["maria143@mail.com", "maria_bosxz@mail.com"],
	introduce: function() {
		console.log("Hello, my name is: " + this.firstName + " " + this.lastName + ". I live in " + this.address.city + ", " + this.address.country + ".")
	}
}

friend.introduce()