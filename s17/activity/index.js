/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:

function printUserInfo() {
	let fullName = "Samuel Faubert";
	let age = 30;
	let location = "Cebu City, Philippines";
	let favouriteDrink = "coffee"
	let height = 178;

	console.log("Hello, I'm " + fullName + ".");
	console.log("I am " + age + " years old.");
	console.log("I live in " + location);
	console.log("My favourite drink is " + favouriteDrink);
	console.log("My height is " + height + "cm");
}
console.log(printUserInfo());


/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:

function printMyFiveBands() {
	let bands = ["Lord Huron", "Rolling Stones", "The Offspring", "Blink 182", "Iron & Wine"];
	console.log(bands[0]);
	console.log(bands[1]);
	console.log(bands[2]);
	console.log(bands[3]);
	console.log(bands[4]);
}
console.log(printMyFiveBands());


/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:

function printMyFiveMovies() {
	let movies = ["Scarface", "The Godfather", "The Good Shepherd", "Tinker Tailor Soldier Spy", "The Dark Knight"];

	console.log(movies[0]);
	console.log(movies[1]);
	console.log(movies[2]);
	console.log(movies[3]);
	console.log(movies[4]);
}
console.log(printMyFiveMovies());

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("These are my friends:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
console.log(printFriends());

// console.log(friend1);
// console.log(friend2);








//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printMyFiveBands,
		printMyFiveMovies,
		printFriends
	}
} catch(err){

}