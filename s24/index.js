
const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2)
console.log(secondNum)

let name = "John"
console.log(`Hello ${name}! Welcome to programming!`)

const message = ` 
 ${name} attended a math competition. He won it by solving the problem 8 ** 2 with the solution of ${secondNum}.
`
console.log(message)

const interestRate = 0.1;
const principal = 1000

console.log(`The interest on your savings account is: ${principal * interestRate}`)

// array destructuring
const fullName = ["Juan", "Dela", "Cruz"]
const [first, middle, last] = fullName;
console.log(`${first} ${middle} ${last}`)

// object destructuring
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

const { givenName, maidenName, familyName } = person 
console.log(`${givenName} ${maidenName} ${familyName}`)

// arrow functions
const hello = () => {
	console.log("Hello World")
}

hello();

const sum = (x, y) => x + y;

console.log(sum(2,3))

const greet = (name = "User") => {
	console.log(`Good morning ${name}`)
}

greet()

// Class-based objects

class Car {
	constructor(brand, model, year) {
		this.brand = brand;
		this.model = model;
		this.year = year
	}
}

const myCar = new Car()
myCar.brand = "Ford"
myCar.model = "Ranger"
myCar.year = 2021

console.log(myCar)

const newCar = new Car("Mitsubishi", "Mirage", 2019)
console.log(newCar)