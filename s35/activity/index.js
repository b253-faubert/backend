const express = require('express');
const mongoose = require('mongoose')
const { Schema } = mongoose;

const port = 4000;
const app = express();

mongoose.connect("mongodb+srv://faubsam20:ieK7MfHETdZE4Rjt@b253-faubert.0egbklz.mongodb.net/s35?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas"));

const userSchema = new Schema({
	name: String,
	password: String
})

const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({ extended: true }))

app.post('/signup', (req, res) => {
	User.findOne({ name: req.body.name }).then((result) => {
		if (result !== null && req.body.name === result.name) {
			return res.send("Duplicate username found");
		} else if (req.body.name === null || req.body.password === null) {
			return res.send("BOTH username and password must be provided")
		} else if (req.body.name !== null && req.body.password !== null) {
			let newUser = new User({
				name: req.body.name,
				password: req.body.password
			});

			newUser.save().then((result) => {
				return res.status(201).send(`New user registered: ${result}`)
			}).catch(err => res.send(err))
		} 
	}).catch(err => res.send(err))
})




app.listen(port, () => {
	console.log(`Listening on port ${port}.....`)
})