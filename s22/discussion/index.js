
// Array methods

let fruits = ["Apple", "Banana", "Orange", "Mango"]

// Push
console.log("Current fruits array: ")
console.log(fruits)
fruits.push("Pineapple")
console.log("Mutated array: ")
console.log(fruits)

// Pop

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DK"];
let countries2 = ["BR", "DE", "UK", "JP"]
console.log(countries)
console.log(countries.indexOf("PH"))
// start counting at index 2
console.log(countries.indexOf("PH", 2))
console.log(countries.indexOf("PH", 6))
console.log(countries.indexOf("DK",-1))

console.log(countries.lastIndexOf("PH"))

console.log("slicing: ")
console.log(countries.slice(2))

console.log(countries.slice(1, 4))

console.log(countries.slice(-3))

console.log("toString")
console.log(countries.toString())

console.log("concat")
console.log(countries.concat(countries2))

console.log(countries.forEach((element, index) => {
	// statements
	console.log(element)
}));

let numbers = [1, 2, 3, 4, 5]
console.log(numbers.map((el) => {
	return el * 2
}))

console.log("every")
console.log(numbers.every((el) => {
	return el > 0;
}))

console.log(numbers.every((el) => {
	return el > 2;
}))

console.log("some")
console.log(numbers.some((el) => {
	return el === 0;
}))

console.log(numbers.some((el) => {
	return el === 2;
}))

console.log("filter")
console.log(numbers.filter((el) => {
	return el > 3
}))

console.log("includes")
console.log(numbers.includes(5))
console.log(numbers.includes(100))

// reduce
/*
- Evaluates elements from left to right and returns/reduces the array into a single value
        - Syntax
            let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
                return expression/operation
            })
        - The "accumulator" parameter in the function stores the result for every iteration of the loop
		reduce()
		- The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
        - How the "reduce" method works
            1. The first/result element in the array is stored in the "accumulator" parameter
            2. The second/next element in the array is stored in the "currentValue" parameter
            3. An operation is performed on the two elements
            4. The loop repeats step 1-3 until all elements have been worked on
*/
let iteration = 0
let reduceArray = numbers.reduce(function(acc, el) {
	console.log(`current iteration: ${++iteration}`)
	console.log(`current accumulator: ${acc}`)
	console.log(`current value: ${el}`)
	return acc + el
}, 20.5)

console.log(reduceArray)