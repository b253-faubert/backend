// comments: 
/*
	multi-line comment
*/

// Variable declaration

let myVar;
console.log(myVar)

const name = "Sam"
const country = "Philippines"
const greeting = "My name is " + name + " and I live in the " + country + "."

console.log(greeting);

let mailAddress = "Metro Cebu \n\n Philippines"

//exponents
let expo = 2e10

// typeof operator
console.log(typeof { name: 'sam', age: 30})