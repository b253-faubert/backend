// Mongo 3T CRUD operations

// Create documents
/*
	db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			});

		Insert Many Documents
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}
			]);
*/
db.users.insertOne({
	"firstName": "Sam",
	"lastName": "Faubert",
	"mobileNumber": "+63987654321",
	"email": "faubertsamuel@gmail.com",
	"company": "Zuitt"
});

db.users.insertMany([
		{
			firstName: "Jared",
			lastName: "Goff",
			age: 28,
			contact: {
				mobileNumber: "+63987254321",
				email: "jgoff@mail.com",
			},
			company: "Detroit Lions"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "15467891232",
				email: "neil@mail.com"
			},
			courses: ["Laravel", "React", "Sass"],
			department: "none"
		}
	])

// Read documents
/*
	db.<collectionName>.find(); -- find all in the collection
	db.<collectionName>.find({ key: value }) -- find all in the collection that match the search parameters
	db.<collectionName>.findOne(); -- find the first document
	db.<collectionName>.findOne({ key: value }) -- find the first document in the collection that matches 
*/

db.users.find();
db.users.find({ firstName: "Jared"}).pretty();

/*
	Mini activity
*/

db.courses.insertMany([
	{
		name: "Javascript 101",
		price: 5000,
		description: "Introduction to Javascript",
		isActive: true 
	},
	{
		name: "HTML 101",
		price: 2000,
		description: "Introduction to HTML",
		isActive: true 
	}
]);

/*
	Update/Modify Documents

	db.<collectionName>.updateOne(
		{
		 field: value
		}, 
		{
			$set: {
				fieldToBeUpdated: value
			}	
		}
	); -- update the first match based on the search

	db.<collectionName>.updateMany(
		{
		 field: value
		}, 
		{
			$set: {
				fieldToBeUpdated: value
			}	
		}
	); -- update all documents that match
*/

db.users.insertOne({
	"firstName": "test",
	"lastName": "test",
	"mobileNumber": "+63987652821",
	"email": "test@gmail.com",
	"company": "none"
});

db.users.updateOne(
	{
		"firstName": "test"
	},
	$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"email": "bg@mail.com",
			"company": "Microsoft",
			"status": "active"
		}
);

db.courses.updateOne(
	{
		name: "HTML 101"
	},
	{
		$set: {
			isActive: false
		}
	}

);
db.courses.updateMany(
	{},
	{
		$set: {
			enrollees: 10
		}
	}

);

// Remove field
db.users.updateOne(
	{
		firstName: "Bill"
	},
	{
		$unset: {
			status: "active"
		}
	}
);

//Delete documents
/*
	Syntax:
		Deleting One Document:
			db.collectionName.deleteOne({"critera": "value"})

		Deleting Multiple Documents:
			db.collectionName.deleteMany({"criteria": "value"})
*/
