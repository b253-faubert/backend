
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoute')
const courseRoutes = require('./routes/courseRoute')

const app = express();
const port = process.env.port || 4000

mongoose.connect("mongodb+srv://faubsam20:ieK7MfHETdZE4Rjt@b253-faubert.0egbklz.mongodb.net/course-booking-API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas"));

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use("/users", userRoutes)
app.use("/courses", courseRoutes)

if (require.main === module) {
	app.listen(port, () => {
		console.log(`API listening on port ${port}`)
	})
}

module.exports = app;