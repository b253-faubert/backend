const mongoose = require('mongoose')
const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Course = require('../models/Course')

module.exports.checkEmailExists = (body) => {
	return User.find({ email: body.email })
			.then((result) => {
				if(result.length > 0) {
					return true
				} else {
					return false
				}
			})
			.catch(err => err)
};

module.exports.registerUser = (body) => {
	const newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		// 10 is the number of salting rounds performed
		password: bcrypt.hashSync(body.password, 10),
		isAdmin: body.isAdmin,
		mobileNo: body.mobileNo,
		enrollments: body.enrollments
	})

	return newUser.save().then(result => result)
				.catch(err => err)
}

module.exports.loginUser = (body) => {
	return User.findOne({ email: body.email })
			.then((result) => {
				if (result === null) {
					return false
				} else {
					const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)
					if (isPasswordCorrect) {
						return { access: auth.createAccessToken(result) }
					} else {
						return false
					}
				}
			}).catch(err => err)
}

module.exports.getProfile = (data) => {
	return User.findById(data.userId)
			.then(result => {
				result.password = ""
				return result
			}).catch(err => err)
}

module.exports.enroll = async (data) => {
	let userToEnroll = await User.findById(data.userId)
	userToEnroll.enrollments.push({
		courseId: data.courseId
	})
	let isUserUpdated = userToEnroll.save()
							.then(user => true)
							.catch(err => false)

	let courseToUpdate = await Course.findById(data.courseId)
	courseToUpdate.enrollees.push({
		userId: data.userId
	})
	let isCourseUpdated = courseToUpdate.save()
							.then(course => true)
							.catch(err => false)

	if (isCourseUpdated && isUserUpdated) {
		return true
	} else {
		return false
	}
}