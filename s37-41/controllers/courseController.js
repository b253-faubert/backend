const mongoose = require('mongoose')
const Course = require('../models/Course')
const bcrypt = require('bcrypt')
const auth = require('../auth')

module.exports.addCourse = (body) => {

	return Course.create({
		name: body.name,
		description: body.description,
		price: body.price
	}).then(result => result)
		.catch(err => err)
}

module.exports.getAllCourses = () => {
	return Course.find()
			.then(result => result)
			.catch(err => err)
}

module.exports.getAllActiveCourses = () => {
	return Course.find({ isActive: true })
			.then(result => result)
			.catch(err => err)
}

module.exports.getCourse = (id) => {
	return Course.findById(id)
			.then(result => result)
			.catch(err => err)
}

module.exports.updateCourse = (id, body) => {
	const updatedCourse = {
		name: body.name,
		description: body.description,
		price: body.price,
		isActive: body.isActive,
		enrollees: body.enrollees
	}
	return Course.findByIdAndUpdate(id, updatedCourse)
			.then(result => result)
			.catch(err => err)
}

module.exports.archiveCourse = (id) => {
	return Course.findById(id)
			.then((result) => {
				result.isActive = false
				return result
			})
			.catch(err => err)
}

