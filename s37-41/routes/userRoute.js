const express = require('express')
const router = express.Router();
const userController = require('../controllers/userController')
const auth = require('../auth')

router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body)
		.then(result => res.send(result))
		.catch(err => res.send(err))
})

router.post('/register', (req, res) => {
	userController.registerUser(req.body)
		.then(result => res.send(result))
		.catch(err => res.send(err))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body)
		.then(result => res.send(result))
		.catch(err => res.send(err))
})


router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		userController.getProfile({ userId: userData.id })
		.then(result => res.send(result))
		.catch(err => res.send(err))
	} else {
		res.send(false)
	}
	
})

router.post('/enroll', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	let data = {
		userId: userData.id,
		isAdmin: userData.isAdmin,
		courseId: req.body.courseId
	}

	if(!data.isAdmin) {
		userController.enroll(data)
			.then(result => res.send(result))
			.catch(err => res.send(err))
	} else {
		res.send(false)
	}

	
})


module.exports = router;