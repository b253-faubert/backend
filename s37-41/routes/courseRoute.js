const express = require('express')
const router = express.Router();
const courseController = require('../controllers/courseController')
const auth = require('../auth')

router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if (!userData.isAdmin) {
		res.send("Must be admin to add a course")
	}

	courseController.addCourse(req.body)
		.then(result => res.send(result))
		.catch(err => res.send(err))
})

router.get('/all', auth.verify, async (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin) {
		try {
			const coursesList = await courseController.getAllCourses();
			return res.send(coursesList)
		} catch (err) {
			return res.send(err)
		}
	} else {
		return res.send(false)
	}
	
})

router.get('/', async (req, res) => {
	try {
		const activeCourses = await courseController.getAllActiveCourses()
		return res.send(activeCourses)
	} catch (err) {
		return res.send(err)
	}
})

router.get('/:id', async (req, res) => {
	try {
		const course = await courseController.getCourse(req.params.id)
		return res.send(course)
	} catch(err) {
		return res.send(err)
	}
})

router.put('/:id', auth.verify, async (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if (userData.isAdmin) {
		try {
			const courseToUpdate = await courseController.updateCourse(req.params.id, req.body);
			const updatedCourse = await courseController.getCourse(req.params.id)
			return res.send(updatedCourse)
		} catch(err) {
			return res.send(err)
		}
	} else {
		return res.send(false)
	}
})

router.patch('/:id/archive', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if (userData.isAdmin) {
		courseController.archiveCourse(req.params.id)
			.then(result => res.send(result))
			.catch(err => res.send(err))
	} else {
		return res.send(false)
	}
})



module.exports = router