const express = require('express');
const router = express.Router();
const taskController = require('../controller/taskController')

router.get('/', (req, res) => {
	taskController.getAllTasks()
		.then(result => res.status(200).send(result));
}) 

router.get('/:id', (req, res) => {
	taskController.getTask(req.params.id)
		.then(result => res.status(200).send(result))
})

router.post('/', (req, res) => {
	taskController.createTask(req.body)
		.then(result => res.status(201).send(result))
})

router.delete('/:id', (req, res) => {
	taskController.deleteTask(req.params.id)
		.then(result => res.send(result))
});

router.put('/:id', (req, res) => {
	taskController.updateTask(req.params.id, req.body)
		.then(result => res.send(result))
})

router.put('/:id/complete', (req, res) => {
	taskController.completeTask(req.params.id)
		.then(result => res.send(result))
})

module.exports = router;