
const Task = require('../models/Task')


module.exports.getAllTasks = () => {
	return Task.find({})
		.then(result => result)
		.catch(err => err)
}

module.exports.getTask = (id) => {
	return Task.findById(id)
		.then(result => result)
		.catch(err => err)
}

module.exports.createTask = (body) => {
	const task = new Task({
		name: body.name,

	})
	return task.save()
		.then(result => task)
		.catch(err => err)
}

module.exports.deleteTask = (id) => {
	return Task.findByIdAndRemove(id)
		.then(task => task)
		.catch(err => err)
}

module.exports.updateTask = (id, body) => {
	return Task.findById(id)
		.then((result) => {
			result.name = body.name
			return result.save().then(result => result)
					.catch(err => err)
		})
		.catch(err => err)
}

module.exports.completeTask = (id) => {
	return Task.findById(id)
		.then((result) => {
			result.status = "complete"
			return result.save().then(result => result)
					.catch(err => err)
		})
		.catch(err => err)
}