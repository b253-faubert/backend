const express = require('express');
const mongoose = require('mongoose');
const taskRoute = require('./routes/taskRoute')

const { Schema } = mongoose
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://faubsam20:ieK7MfHETdZE4Rjt@b253-faubert.0egbklz.mongodb.net/s35?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas"));

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use("/tasks", taskRoute)


if (require.main === module) {
	app.listen(4000, () => {
		console.log(`Listening on port ${port}.....`)
	})
}

module.exports = app