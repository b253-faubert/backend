console.log('hello world')
console.log('');

function printInput() {
	let nickname = prompt("Enter your nickname: ")
	console.log("Hi, " + nickname)
}

//printInput();

function printName(name) {
	console.log("My name is: " + name)
}
printName("Samuel")

let sampleName = "Michelle"
printName(sampleName);

function checkDivisibilityBy8(num) {
	return "Is " + num + " divisible by 8?: " + (num % 8 === 0);
}

console.log(checkDivisibilityBy8(64));
console.log(checkDivisibilityBy8(28));

function argumentFunction() {
	console.log("This function was passed as an argument before the printed message");
}

function invokeFunction(argFunc) {
	argFunc();
}

invokeFunction(argumentFunction);

function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName)
}

createFullName("Sam", "Arthur", "Faubert")

let last = "Smith"
let middle = "S."
let first = "John"

createFullName(first, middle, last)

function printPlayerInfo(username, level, jobClass) {
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job class: " + jobClass);
}

let user1 = printPlayerInfo("sam", 95, "reaper");
console.log(user1)