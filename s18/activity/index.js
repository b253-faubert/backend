
function addNum(num1, num2) {
	console.log(num1 + num2);
}

function subNum(num1, num2) {
	console.log(num1 - num2);
}

console.log("Displayed sum of 3 and 4")
addNum(3, 4);
console.log("Displayed difference of 5 and 2")
subNum(5, 2);

function multiplyNum(num1, num2) {
	return num1 * num2;
}

function divideNum(num1, num2) {
	return num1 / num2;
}

let product = multiplyNum(3,5);
let quotient = divideNum(20, 4);

console.log("The product of 3 and 5:")
console.log(product)
console.log("The quotient of 20 and 4:")
console.log(quotient)

function getCircleArea(rad) {
	return 3.1416 * (rad**2);
}

let circleArea = getCircleArea(15);
console.log("The result of getting the area of a circle with 15 radius:")
console.log(circleArea);

function getAverage(num1, num2, num3, num4) {
	return (num1+num2+num3+num4) / 4;
}

let averageVar = getAverage(1,2,3,6);
console.log("The average of 1, 2, 3, and 6:")
console.log(averageVar);

function checkIfPassed(score, total) {
	let percentage = score/total;
	let isPassed = percentage > 0.75;
	return isPassed;
}

let isPassingScore = checkIfPassed(77, 100);
console.log("Is 77/100 a passing score?")
console.log(isPassingScore);


//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}