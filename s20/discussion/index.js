
let count = 5;

while (count !== 0) {
	console.log("While " + count)
	count--;
}

// let number = Number(prompt("Give me a number")) 
// do {
// 	console.log("Do while " + number)
// 	number += 1;
// } while(number < 10)

let name = "Samuel"
let vowels = "aeiou"
let vowelsCount = 0;

for(let i = 0; i < name.length; i++) {
	if(vowels.includes(name[i])) {
		vowelsCount += 1;
	}
}

console.log("Vowels in " + name + " : " + vowelsCount)