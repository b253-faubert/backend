db.inventory.insertMany([
		{
			"name": "Javascript for Beginners",
			"author": "James Doe",
			"price": 5000,
			"stocks": 50,
			"publisher": "JS Publishing House"
		},
		{
			"name": "HTML and CSS",
			"author": "John Thomas",
			"price": 2500,
			"stocks": 38,
			"publisher": "NY Publishers"
		},
		{
			"name": "Web Development Fundamentals",
			"author": "Noah Jimenez",
			"price": 3000,
			"stocks": 10,
			"publisher": "Big Idea Publishing"
		},
		{
			"name": "Java Programming",
			"author": "David Michael",
			"price": 10000,
			"stocks": 100,
			"publisher": "JS Publishing House"
		}
	]);

// Comparison query operators
// $gt and $gte, $lt and $lte

db.inventory.find({
	stocks: {
		$gte: 50
	}
});

db.inventory.find({
	stocks: {
		$gt: 50
	}
});

db.inventory.find({
	"stocks": {
		$lt: 50
	}
});

db.inventory.find({
	"stocks": {
		$lte: 50
	}
});

// not equal to query parameter
db.inventory.find({
	"stocks": {
		$ne: 50
	}
});

// equal operator

db.inventory.find({
	"stocks": {
		$eq: 50
	}
});

// match any values specified in an array. returns all documents matching 1 parameter
db.inventory.find({
	"price": {
		$in: [10000, 5000]
	}
});

//match not in
db.inventory.find({
	"price": {
		$nin: [10000, 5000]
	}
});

// logical AND

db.inventory.find({
	"price": {
		$gt: 2000
	} && {
		$lt: 4000
	}
});

// mini-activity
db.inventory.find({
	"price": {
		$lte: 4000
	}
});

db.inventory.find({
	"stocks": {
		$in: [50, 100]
	}
});

// logical OR
db.inventory.find({
	$or: [
		{ "name": "HTML and CSS"},
		{ "publisher": "JS Publishing House"}
	]
});

db.inventory.find({
	$or: [
		{ "author": "James Doe"},
		{ "price": {
			$lte: 5000
		}}
	]
});

// $and operator

db.inventory.find({
	$and: [
		{
			"stocks": {
				$ne: 50
			},
			"price": {
				$ne: 5000
			}
		}
	]
});

/*
	Field projection
	- inclusion: return included fields ( 1 for true )
	- exclusion: do not return listed fields ( 0 for false )
*/

db.inventory.find({
	"publisher": "JS Publishing House"
}, {
	"name": 1,
	"author": 1,
	"price": 1
	}
);

db.inventory.find({
	"author": "Noah Jimenez"
}, {
	"stocks": 0,
	"price": 0
	}
);

db.inventory.find(
{
	"price": {
		$lte: 5000
	}
},
{
	"_id": 0,
	"name": 1,
	"author": 1
}
);

// Evaluation query operator $regex

db.inventory.find({
	"author": {
		$regex: 'M',
		$options: '$i'
	}
});

//mini-activty
db.inventory.find({
	$and:[
	{
		"name": {
			$regex: 'java',
			$options: '$i'
		}
	},
	{
		"price": {
			$gte: 5000
		}
	}
	]
});

db.inventory.updateOne({
		"price": {
			$gte: 3000
		}
	},
	{
		$set: {
			"stocks": 100
		}
	}
);

db.inventory.deleteOne({
	"price": {
		$gte: 3000
	}
}
)